import json
from flask import request

from utils.sendRequests import SendRequests
from utils.logger import LogInfo


class FacebookConfigController(SendRequests):

  def validateProperty (self,property):
    propertiesAllowed = ['get_started', 'persistent_menu', 'greeting', 'ice_breakers']

    if property not in propertiesAllowed:
      return None

    return property.lower()

  def createProperties(self):
    property = self.validateProperty(request.get_json()['property'])

    if property is None:
      return "La propiedad enviada no esta permitida"

    else:
      data = self.templateProperties(property)
      return self.post('facebook', 'me/messenger_profile', data)


  def deleteProperties(self):
    property = self.validateProperty(request.get_json()['property'])

    if property is None:
      return "La propiedad enviada no esta permitida"

    else:
      data = self.templateDeleteProperties(property)
      return self.delete('facebook', 'me/messenger_profile', data)


  def templateProperties(self, data):
    property = data['property']

    if property == "get_started":
      payload = data['payload']
      return {
        "get_started":{
                "payload":payload
            }
      }

    if property == "greeting":
      message = data['message']
      return {
      "greeting":[
              {
                  "locale":"default",
                  "text":message
              }
          ]
      }

    if property == "persistent_menu":
      actions = data['actions']
      return {
                  "persistent_menu": [
              {
                  "locale": "default",
                  "composer_input_disabled": False,
                  "call_to_actions": actions
              }
          ]
      }

    if property == "ice_breakers":
      breakers = data['breakers']
      return {
            "ice_breakers":breakers
           }

  def templateDeleteProperties(self, property):
    options = [property]

    if property == "get_started":
      options = ["persistent_menu","get_started"]

    return {
      "fields": options
    }

