from flask import Blueprint, request

# Import Facebook e instanciando la clase
from controllers.FacebookConfigController import FacebookConfigController
facebookConfig = FacebookConfigController()

# Registrando en Blueprint
facebookConfigBP = Blueprint('FacebookConfigController', __name__)

# Route para validar el webHook
@facebookConfigBP.route('/propierties', methods=['POST'])
def createProperties():
    return facebookConfig.createProperties()

@facebookConfigBP.route('/propierties', methods=['DELETE'])
def deleteProperties():
    return facebookConfig.deleteProperties()

